import Funfun
import System.Exit

-- --------------------------------------------------------------------------- 

isEqualXY threshold (expectedX, expectedY) (actualX, actualY) =
  abs (expectedX - actualX) <= threshold && abs (expectedY - actualY) <= threshold

-- zip doesn't require the lists be equal length, so we add an extra assertion.
isEqualListXY threshold expected actual = 
  length expected == length actual && all (\(e, a) -> isEqualXY threshold e a) (zip expected actual)

-- --------------------------------------------------------------------------- 

assertGeneralEquals status message isEqual expected actual = do
  if isEqual expected actual then
    return ()
  else do
    putStrLn message
    putStr $ "  Expected: "
    print expected
    putStr $ "    Actual: "
    print actual
    System.Exit.exitWith (ExitFailure status)
    return ()

assertEquals message expected actual = 
  assertGeneralEquals 2 message (==) expected actual

assertMandatoryEquals message expected actual = 
  assertGeneralEquals 1 message (==) expected actual

assertAlmostEquals message threshold expected actual = 
  assertGeneralEquals 2 message (isEqualXY threshold) expected actual

assertListAlmostEquals message threshold expected actual = 
  assertGeneralEquals 2 message (isEqualListXY threshold) expected actual

assertTrue message actual = assertEquals message True actual
assertFalse message actual = assertEquals message False actual

-- --------------------------------------------------------------------------- 

assertTwoByTwo l expected =
  assertMandatoryEquals ("twoByTwo" ++ (show l) ++ " gives an unexpected value.") expected $ twoByTwo l

assertSumOfSquares l expected =
  assertMandatoryEquals ("sumOfSquares " ++ (show l) ++ " gives an unexpected value.") expected $ sumOfSquares l

assertSquareOfSum l expected =
  assertMandatoryEquals ("squareOfSum " ++ (show l) ++ " gives an unexpected value.") expected $ squareOfSum l

assertSqumDiff l expected =
  assertMandatoryEquals ("squmDiff " ++ (show l) ++ " gives an unexpected value.") expected $ squmDiff l

assertMode l expected =
  assertEquals ("mode " ++ (show l) ++ " gives an unexpected value.") expected $ mode l

assertIndices prefix l expected =
  assertEquals ("indices " ++ (show prefix) ++ " " ++ (show l) ++ " gives an unexpected value.") expected $ indices prefix l

assertNroutes c r expected =
  assertEquals ("nroutes " ++ show c ++ " " ++ show r ++ " gives an unexpected value.") expected $ nroutes c r

assertFlatten2 l expected =
  assertEquals ("flatten " ++ show l ++ " gives an unexpected value.") expected $ flatten2 l

assertTranslate factors point expected =
  assertAlmostEquals ("translate " ++ show factors ++ " " ++ show point ++ " gives an unexpected value.") 0.001 expected $ translate factors point

assertScale factors point expected =
  assertAlmostEquals ("scale " ++ show factors ++ " " ++ show point ++ " gives an unexpected value.") 0.001 expected $ scale factors point

assertRotate angle point expected =
  assertAlmostEquals ("rotate " ++ show angle ++ " " ++ show point ++ " gives an unexpected value.") 0.001 expected $ rotate angle point

assertTransformAll operation f l expected =
  assertListAlmostEquals ("transformAll on " ++ show l ++ " with a function that " ++ operation ++ " gives an unexpected value.") 0.001 expected $ transformAll f l

assertGauntlet operations fs p expected =
  assertAlmostEquals ("gauntlet on " ++ show p ++ " with operations that " ++ operations ++ " gives an unexpected value.") 0.001 expected $ gauntlet fs p

assertGauntletAll operations fs l expected =
  assertListAlmostEquals ("gauntletAll on " ++ show l ++ " with operations that " ++ operations ++ " gives an unexpected value.") 0.001 expected $ gauntletAll fs l

-- --------------------------------------------------------------------------- 

main = do
  assertTwoByTwo [1..2] [(1, 2)]
  assertTwoByTwo [1..3] [(1, 2)]
  assertTwoByTwo [1..4] [(1, 2), (3, 4)]
  assertTwoByTwo [1..5] [(1, 2), (3, 4)]
  assertTwoByTwo "abcdefgh" [('a', 'b'), ('c', 'd'), ('e', 'f'), ('g', 'h')]
  assertTwoByTwo ([] :: [Integer]) [] -- empty lists are ambiguous

  assertMode [1, 1, 1] 1
  assertMode [2, 2, 1] 2
  assertMode ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'z', 'z'] 'z'
  assertMode ([0..10] ++ [-10..0]) 0

  assertSumOfSquares [1..10] 385
  assertSumOfSquares [1..3] 14
  assertSumOfSquares [1..4] 30
  assertSumOfSquares [1..5] 55
  assertSumOfSquares [2..5] 54
  assertSumOfSquares [100..102] 30605
  assertSumOfSquares [-4..(-2)] 29
  assertSumOfSquares [3, 5, 7] (9 + 25 + 49)
  assertSumOfSquares [0, 0, 0, 0] 0
  assertSumOfSquares [10,12..20] 1420

  assertSquareOfSum [1, 5, 10] 256
  assertSquareOfSum [1..100] (5050 ^ 2)
  assertSquareOfSum [1..3] 36
  assertSquareOfSum [0] 0
  assertSquareOfSum [0, 0, 0, 0] 0
  assertSquareOfSum [10, 20, 21, 1, 1, 1] 2916
  assertSquareOfSum [10,12..20] 8100

  assertSqumDiff 5 170
  assertSqumDiff 3 22
  assertSqumDiff 10 2640
  assertSqumDiff 20 41230
  assertSqumDiff 0 0
  assertSqumDiff 1 0
  assertSqumDiff 2 4

  assertIndices "zo" "bozo bozo" [2, 7]
  assertIndices "iss" "Mississippi" [1, 4]
  assertIndices [1, 7, 9] [1, 7, 9, 3, 8, 1, 7, 9, 10, 1, 6, 1, 7, -3, 1, 7, 9, 1, 7] [0, 5, 14]
  assertIndices [(5, 6)] [(5, 6), (5, 6), (5, 6)] [0..2]
  assertIndices "" "abc" [0..3]
  assertIndices "abc" "" []

  assertNroutes 1 1 2
  assertNroutes 2 1 3
  assertNroutes 3 1 4
  assertNroutes 4 1 5
  assertNroutes 2 2 6
  assertNroutes 7 9 11440
  assertNroutes 5 4 126
  assertNroutes 5 15 15504
  assertNroutes 10 10 184756
  assertNroutes 100 100 90548514656103281165404177077484163874504589675413336841320
  assertNroutes 94 87 158654935209831458628307974512092759234129458397116000

  assertFlatten2 [[1]] [1]
  assertFlatten2 [[1, 2, 3]] [1, 2, 3]
  assertFlatten2 [[1, 2, 3], [10, 13]] [1, 2, 3, 10, 13]
  assertFlatten2 [[1, 2, 3], [10, 13], [100]] [1, 2, 3, 10, 13, 100]
  assertFlatten2 ["germ", "i", "nate"] "germinate"
  assertFlatten2 [[(3, 5)], [(10, 3)], [(1, 1), (50, 100)]] [(3, 5), (10, 3), (1, 1), (50, 100)]

  assertTranslate (5, 1) (0, 0) (5, 1)
  assertTranslate (5, 1) (3, 0) (8, 1)
  assertTranslate (100, 200) (-50, -10) (50, 190)

  assertScale (1, 1) (4, 5) (4, 5)
  assertScale (0.5, 0.25) (100, 100) (50, 25)
  assertScale (5, 0) (10, 20) (50, 0)
  assertScale (-1, -2) (100, 200) (-100, -400)
  assertScale (0.1, 0.5) (-100, -200) (-10, -100)
  assertScale (0, 0) (1000, 1001) (0, 0)

  assertRotate 90 (1, 2) (-2, 1)
  assertRotate 37 (1, 2) (-0.4049945362568037,2.199086043246634)
  assertRotate 180 (5, 10) (-5, -10)
  assertRotate 360 (8, 100) (8, 100)
  assertRotate 1 (1000, 2000) (964.9428822818243,2017.1477967500662)
  assertRotate (-17) (50, 63) (66.23465519568418,45.6286143895344)

  assertTransformAll "adds 1 to the x-components" (\(x, y) -> (x + 1, y)) [(5, 9)] [(6, 9)] 
  assertTransformAll "zeroes all the y-components" (\(x, y) -> (x, 0)) [(1, 2), (7, 10), (-4, -10), (100, 2000)] [(1, 0), (7, 0), (-4, 0), (100, 0)]
  assertTransformAll "flips the xy-components" (\(x, y) -> (y, x)) [(1, 2), (-5, -7)] [(2, 1), (-7, -5)]
  assertTransformAll "scales by (100, 200)" (scale (100, 200)) [(3, 4), (1, 2), (-1, -1), (1, 1)] [(300, 800), (100, 400), (-100, -200), (100, 200)]

  assertGauntlet "translates by (3, 4) and then scales by (2, 7)" [translate (3, 4), scale (2, 7)] (1, 1) (8, 35)
  assertGauntlet "translates by (3, 4) and then scales by (2, 7)" [translate (3, 4), scale (2, 7)] (3, 4) (12, 56)
  assertGauntlet "scales by (-1, 1), translates by (-2, 0), and then rotates by 45 degrees" [scale (-1, 1), translate (-2, 0), rotate 45] (10, 7) (-13.435028842544403,-3.535533905932737)
  assertGauntlet "rotates by 45 degrees and then rotates by -45 degrees" [rotate 45, rotate (-45)] (91, 87) (91, 87)
  assertGauntlet "scales by (5, 3.5), rotates by 22 degrees, and then translates by (4.1, 6.2)" [scale (5, 3.5), rotate 22, translate (4.1, 6.2)] (13.2, 17.8) (41.95614363159665,88.68758930496105)
  assertGauntlet "do nothing" [] (5, 6) (5, 6)

  assertGauntletAll "do nothing" [] [(1.5, 4.5)] [(1.5, 4.5)]
  assertGauntletAll "scale by (1, 2) and then translate by (4, 5)" [scale (1, 2), translate (4, 5)] [] []
  assertGauntletAll "scale by (1, 2) and then translate by (4, 5)" [scale (1, 2), translate (4, 5)] [(1.5, 6.2)] [(5.5, 17.4)]
  assertGauntletAll "rotate by 45 degrees, rotate by 1 degree, and then rotate by 3 degrees" [rotate 45, rotate 1, rotate 3] [(6, 7), (9, 10), (15, 4), (5, 0)] [(-1.3466128876163588,9.120670684270184),(-1.642564541313153,13.352976511910022),(6.822047113966522,13.944879819303608),(3.2802951449525364,3.7735479011138593)]
  assertGauntletAll "translate by (4, 8.2), scale by (0.1, 0.5), and then rotate by -43 degrees" [translate (4, 8.2), scale (0.1, 0.5), rotate (-43)] [(1, 2), (6.7, 8.9), (4, -4), (pi, pi * 2), (100, 200)] [(3.843868487128327,3.3889046982265207),(6.6136344392668756,5.523335903577035),(2.017279517426583,0.9902440853502592),(5.461057336254792,4.809110145022824),(78.60210777934546,69.04113739390567)]
