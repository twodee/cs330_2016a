#!/usr/bin/env ruby

require 'minitest/autorun'
require 'bleakrvm'
require 'json'

class TestBleakrVM < MiniTest::Test
  def test_default
    vm = BleakrVM.new '/dev/null', '/dev/null'
    assert_empty(vm.labels, 'I asked a VM with empty source for its labels. I expected there to be none, but that\'s not you gave me')
    assert_empty(vm.instructions, 'I asked a VM with empty source for its instructions. I expected there to be none, but that\'s not what you gave me')
    assert_empty(vm.input, 'I asked a VM with empty source for its input. I expected there to be none, but that\'s not what you gave me')
    assert_raises(RuntimeError, 'I asked a VM with empty to take a step, but I didn\'t get the exception I expected to be raised') { vm.step }
  end

  def test_resolve
    tests_dir = File.join(File.dirname(__FILE__), 'tests')
    vm = BleakrVM.new(File.join(tests_dir, 'resolve.bleakr'), File.join(tests_dir, 'blank.in'))

    10.times do |i|
      steps = "step#{i == 1 ? '' : "s"}"
      assert_equal(100, vm.resolve_rvalue('100'), "I tried resolving rvalue '100' after running #{i} #{steps} of a VM on test case resolve.bleakr, but I didn't get back the value I expected.")
      assert_equal(9, vm.resolve_rvalue('9'), "I tried resolving rvalue '9' after running #{i} #{steps} of a VM on test case resolve.bleakr, but I didn't get back the value I expected.")
      assert_equal(-23, vm.resolve_rvalue('-23'), "I tried resolving rvalue '-23' after running #{i} #{steps} of a VM on test case resolve.bleakr, but I didn't get back the value I expected.")

      # The ones we've processed so far.
      (0...i).each do |n|
        assert_equal("r#{n}", vm.resolve_lvalue("r#{n}"), "I tried resolving lvalue 'r#{n}' after running #{i} #{steps} of a VM on test case resolve.bleakr, but I didn't get back the value I expected.")
        assert_equal("r#{9 - n}", vm.resolve_lvalue("[r#{n}]"), "I tried resolving lvalue '[r#{n}]' after running #{i} #{steps} of a VM on test case resolve.bleakr, but I didn't get back the value I expected.")
        assert_equal(9 - n, vm.resolve_rvalue("r#{n}"), "I tried resolving rvalue 'r#{n}' after running #{i} #{steps} of a VM on test case resolve.bleakr, but I didn't get back the value I expected.")
        assert_equal((9 - n) >= i ? 0 : n, vm.resolve_rvalue("[r#{n}]"), "I tried resolving rvalue '[r#{n}]' after running #{i} #{steps} of a VM on test case resolve.bleakr, but I didn't get back the value I expected.")
      end

      # The untouched so far.
      (i...10).each do |n|
        assert_equal("r#{n}", vm.resolve_lvalue("r#{n}"), "I tried resolving lvalue 'r#{n}' after running #{i} #{steps} of a VM on test case resolve.bleakr, but I didn't get back the value I expected.")
        assert_equal('r0', vm.resolve_lvalue("[r#{n}]"), "I tried resolving lvalue '[r#{n}]' after running #{i} #{steps} of a VM on test case resolve.bleakr, but I didn't get back the value I expected.")
        assert_equal(0, vm.resolve_rvalue("r#{n}"), "I tried resolving rvalue 'r#{n}' after running #{i} #{steps} of a VM on test case resolve.bleakr, but I didn't get back the value I expected.")
        assert_equal(i > 0 ? 9 : 0, vm.resolve_rvalue("[r#{n}]"), "I tried resolving rvalue '[r#{n}]' after running #{i} #{steps} of a VM on test case resolve.bleakr, but I didn't get back the value I expected.")
      end
      vm.step
    end
  end

  def test_cases
    tests_dir = File.join(File.dirname(__FILE__), 'tests')

    tests = [
      ['arithmetic', [['blank', 'arithmetic']]],
      ['count_by_3_to_40', [['blank', 'count_by_3_to_40']]],
      ['indirect', [['blank', 'indirect']]],
      ['jzilch', [['blank', 'jzilch']]],
      ['jzilch_not', [['blank', 'jzilch_not']]],
      ['eg1', [['eg1', 'eg1']]],
      ['zero_for_each_input', [['zero_for_each_input', 'zero_for_each_input']]],
      ['samesies', [['samesies', 'samesies']]],
      ['sign', [['sign', 'sign']]],
      ['decrement_all', [['decrement_all', 'decrement_all']]],
      ['tail8', [['tail8_10', 'tail8_10'], ['tail8_57', 'tail8_57']]]
    ]

    tests.each do |test, inouts|
      
      source_path = File.join(tests_dir, test + '.bleakr')

      inouts.each do |input, expected|
        input_path = File.join(tests_dir, input + '.in')
        expected_path = File.join(tests_dir, expected + '.json')

        expected_states = JSON.parse(IO.read(expected_path))

        vm = BleakrVM.new source_path, input_path

        2.times do |isweep|
          i = 0
          test_label = "#{test}#{isweep == 1 ? " (run once and then reset)" : ""}"
          assert_state vm, expected_states[i], test_label, i
          while true
            i = i + 1
            if i >= expected_states.length
              assert_raises(RuntimeError, "I ran a VM executing test #{test_label} through the point where I expected an exception, but I didn't get the exception I was expecting.") { vm.step }
              # Try stepping a few more times before calling it quits on this case.
              break if i > expected_states.length + 20
            else
              vm.step
              assert_state vm, expected_states[i], test, i
            end
          end
          vm.reset
        end
      end
    end
  end

  def assert_state vm, state, test, i
    assert_equal(state['input'], vm.input, "I asked a VM executing test #{test} for its input array after #{i} step#{i == 1 ? '' : 's'}, but I didn't get back what I expected")
    assert_equal(state['output'], vm.output, "I asked a VM executing test #{test} for its output array after #{i} step#{i == 1 ? '' : 's'}, but I didn't get back what I expected")
    assert_equal(state['registers'], vm.registers, "I asked a VM executing test #{test} for its registers after #{i} step#{i == 1 ? '' : 's'}, but I didn't get back what I expected")
    assert_equal(state['instructions'], vm.instructions, "I asked a VM executing test #{test} for its instructions array after #{i} step#{i == 1 ? '' : 's'}, but I didn't get back what I expected")
    assert_equal(state['labels'], vm.labels, "I asked a VM executing test #{test} for its labels array after #{i} step#{i == 1 ? '' : 's'}, but I didn't get back what I expected")
  end
end
