#!/usr/bin/env zsh

driver="require './bleakrvm.rb'
require './bleakrdebugger.rb'
BleakrDebugger.new(BleakrVM.new(ARGV[0], ARGV[1]))"

src="store r0 5
jmp skip
store r1 6
store r2 7 <- skip
store [r0] 100
input r7
output r5"

input="204"

rvm use jruby-1.7.19
jruby =(cat <<<$driver) =(cat <<<$src) =(cat <<<$input)
