#!/usr/bin/env ruby

include Java

import java.awt.BasicStroke
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Point
import java.awt.Robot
import java.awt.Toolkit
import java.awt.event.KeyListener
import java.awt.event.KeyEvent
import java.awt.geom.Ellipse2D
import java.awt.geom.Line2D
import java.awt.image.BufferedImage
import javax.swing.JPanel

require_relative 'player'
require_relative 'world'

module Utilities
  TURN_DELTA = 3
  MOVE_DELTA = 0.1

  def draw_overlay g2, x, y, width, height, opacity, rays = []
    lookAtColor = Color.new(0, 255, 0, opacity)
    rayColor = Color.new(128, 128, 128, opacity)
    playerColor = Color.new(255, 0, 255)
    playerRadius = 0.25

    oldTransform = g2.getTransform

    aspectWorld = @world.width / @world.height.to_f
    aspectViewport = width / height.to_f
    fitWidth, fitHeight, scale = if aspectViewport < aspectWorld
      [width, width / aspectWorld, width / @world.width.to_f]
    else
      [height * aspectWorld, height, height * @world.height.to_f]
    end

    # (x, y + height) is the new origin.
    g2.translate(x, y + fitHeight)

    # The y-axis now points up.
    g2.scale(1, -1)

    # Scale the 1-unit blocks to fill the space.
    g2.scale(fitWidth / @world.width.to_f, fitHeight / @world.height.to_f)

    # Scale down the stroke so when transformed, it's still just a pixel wide.
    stroke = g2.getStroke
    g2.setStroke(BasicStroke.new(1.0 / scale))

    @world.height.times do |r|
      @world.width.times do |c|
        cellType = @world[c, r]
        g2.setColor(Color.new(cellType * 200, 50, 0, opacity))
        g2.fillRect(c, r, 1, 1)
      end
    end

    # Draw helper rays from player.
    g2.setColor(rayColor)
    rays.each do |ray|
      g2.draw(Line2D::Double.new(@player.position.x, @player.position.y, ray.x, ray.y))
    end

    # Make stroke thicker for the player and lookAt ray.
    g2.setStroke(BasicStroke.new(500.0 / scale))
    target = @player.position + @player.lookat
    g2.setColor(Color::WHITE)
    g2.draw(Line2D::Double.new(@player.position.x, @player.position.y, target.x, target.y))

    # Draw oval around player.
    g2.setColor(playerColor)
    g2.fill(Ellipse2D::Double.new(@player.position.x - playerRadius, @player.position.y - playerRadius, 2 * playerRadius, 2 * playerRadius))

    # Restore panel's original state.
    g2.setTransform(oldTransform)
    g2.setStroke(stroke)
  end

  def setup
    panel = WasdPanel.new(@buffer)
    add(panel, BorderLayout::CENTER)
    @has_overlay = true

    addKeyListener do |e|
      case e.getKeyCode
        when KeyEvent::VK_Q
          @player.rotate(TURN_DELTA)
          draw_all_columns
        when KeyEvent::VK_E
          @player.rotate(-TURN_DELTA)
          draw_all_columns
        when KeyEvent::VK_A
          @player.strafe(-MOVE_DELTA)
          draw_all_columns
        when KeyEvent::VK_D
          @player.strafe(MOVE_DELTA)
          draw_all_columns
        when KeyEvent::VK_W
          @player.advance(MOVE_DELTA)
          draw_all_columns
        when KeyEvent::VK_S
          @player.advance(-MOVE_DELTA)
          draw_all_columns
        when KeyEvent::VK_O
          @has_overlay = !@has_overlay
          draw_all_columns
        when KeyEvent::VK_ESCAPE
          dispose
          @pool.stop
      end
    end

    setDefaultCloseOperation(JFrame::EXIT_ON_CLOSE)
    setSize(@buffer.getWidth, @buffer.getHeight)
    @robot = Robot.new

    dimension = Toolkit.getDefaultToolkit().getScreenSize();
    center = [dimension.width / 2, dimension.height / 2]
    setLocation(center[0] - getSize.width / 2, center[1] - getSize.height / 2);

    addMouseMotionListener do |e|
      if e.getX != getWidth / 2
        diff = getWidth / 2 - e.getX
        @player.rotate 0.1 * diff
        @robot.mouseMove(center[0], center[1])
        draw_all_columns
      end
    end

    image = BufferedImage.new(16, 16, BufferedImage::TYPE_INT_ARGB)
    blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(image, Point.new(0, 0), "blank cursor")
    getContentPane().setCursor(blankCursor)

    setVisible(true)
  end

  class MutableVector2
    attr_accessor :x, :y

    def initialize x, y
      @x = x
      @y = y
    end
  end

  def wall_distance(direction)
    origin = @player.position
    column = origin.x.to_i
    row = origin.y.to_i
    sideDistance = MutableVector2.new 0, 0
    deltaDistance = Vector2.new(Math.sqrt(1 + (direction.y * direction.y) / (direction.x * direction.x)),
                                Math.sqrt(1 + (direction.x * direction.x) / (direction.y * direction.y)))
    perpWallDist = 0
    stepX = 0
    stepY = 0

    if direction.x < 0
      stepX = -1
      sideDistance.x = (origin.x - column) * deltaDistance.x
    else
      stepX = 1
      sideDistance.x = (column + 1 - origin.x) * deltaDistance.x
    end

    if direction.y < 0
      stepY = -1
      sideDistance.y = (origin.y - row) * deltaDistance.y
    else
      stepY = 1
      sideDistance.y = (row + 1 - origin.y) * deltaDistance.y
    end

    isSide = false
    while @world[column, row] == 0
      if sideDistance.x < sideDistance.y
        sideDistance.x += deltaDistance.x
        column += stepX
        isSide = false
      else
        sideDistance.y += deltaDistance.y
        row += stepY
        isSide = true
      end
    end

    if !isSide
      perpWallDist = ((column - origin.x + (1 - stepX) * 0.5) / direction.x).abs
    else
      perpWallDist = ((row - origin.y + (1 - stepY) * 0.5) / direction.y).abs
    end

    [perpWallDist, isSide]
  end

  def draw_one_column g, x
    cameraX = 2 * x / (getWidth - 1).to_f - 1
    angle = @player.fov * -0.5 * cameraX
    direction = @player.lookat.rotate(angle)

    perpWallDist, isSide = wall_distance(direction)

    lineHeight = ((getHeight / perpWallDist)).abs.to_i

    g.setColor(Color.new(0, 0, isSide ? 255 : 128))
    g.drawLine(x, getHeight / 2 - lineHeight / 2, x, getHeight / 2 + lineHeight / 2)

    @player.position + direction * perpWallDist
  end

  class WasdPanel < JPanel
    def initialize(buffer)
      super()
      @buffer = buffer
    end

    def paintComponent g
      g.drawImage(@buffer, 0, 0, nil)
    end
  end
end
