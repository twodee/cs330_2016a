#!/usr/bin/env zsh

driver="require './wasd.rb'
require './player.rb'
require './world.rb'
world = World.new ARGV[0]
player = Player.new world, 60
wasd = Wasd.new player, world"

world="5 4
1.5 1.5
0 1
1 1 1 1 1
1 0 0 0 1
1 0 1 0 1
1 1 1 1 1"

rvm use jruby-1.7.19
jruby =(cat <<<$driver) =(cat <<<$world)
