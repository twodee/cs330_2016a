#!/usr/bin/env ruby

require 'minitest/autorun'
require 'vector2'
require 'world'
require 'player'
require 'threadpool'
require 'json'

module Minitest::Assertions
  def assert_close expected, actual, delta, message
    assert((expected - actual).abs <= delta, "#{message}. Actual value #{actual} is not nearly equal to the expected #{expected}.")
  end
end

class TestWasd < MiniTest::Test
  def test_vector2_initialize
    [[0, 0], [1, 10], [3, -5], [6.8, 100.2]].each do |x, y|
      v = Vector2.new x, y
      assert_close(x, v.x, 1.0e-4, "Vector2.x gave me an unexpected x-value for [#{x}, #{y}]")
      assert_close(y, v.y, 1.0e-4, "Vector2.y gave me an unexpected y-value for [#{x}, #{y}]")
    end
  end

  def test_vector2_rotate
    # g = Random.new
    # 30.times do
      # x = g.rand(-100.0..100.0).round(2)
      # y = g.rand(-100.0..100.0).round(2)
      # degrees = g.rand(-720.0..720.0).round(2)

      # v = Vector2.new(x, y)
      # result = v.rotate degrees

      # puts "[%.2f, %.2f, %.2f, %.10f, %.10f]," % [x, y, degrees, result.x, result.y]
    # end

    tuples = [
      [46.72, 44.45, -471.45, 24.2862648651, -59.7389172894],
      [3.37, -76.71, 635.64, -76.0074535577, -10.8925664414],
      [25.39, 78.99, -421.67, 81.5780919702, 15.1356238889],
      [75.28, 23.65, 687.62, 76.2404153011, -20.3420740023],
      [39.19, -25.26, -460.34, -31.8839528395, -34.0196597767],
      [-23.06, -18.08, 458.33, 21.2300585005, -20.1973913183],
      [-32.58, -26.34, -39.82, -41.8909483433, 0.6328087355],
      [-42.81, -58.63, -634.26, 55.2879834942, -47.0469114943],
      [-92.91, -67.44, 126.68, 109.5851609169, -34.2273897165],
      [35.62, -7.23, -136.20, -30.7132944719, -19.4358134041],
      [-73.09, -49.73, -204.95, 87.2464333781, 14.2576597941],
      [-56.79, -92.71, 664.27, -108.5929932957, -5.2735194197],
      [-35.14, -80.07, 6.13, -26.3888251885, -83.3645872369],
      [42.40, 87.81, 519.05, -70.9937936147, -66.8448754071],
      [-3.81, -58.10, -87.07, -58.2187990379, 0.8351877564],
      [80.51, -7.83, 657.43, 30.1383985988, -75.0656108341],
      [-20.19, 44.28, -373.80, -9.0449396098, 47.8178164229],
      [14.13, 76.55, -232.79, -69.5112362777, -35.0392270341],
      [-61.48, -99.92, -592.08, 116.6070375030, 12.9071919785],
      [-57.67, -71.90, -45.83, -91.7559698054, -8.7338940380],
      [-43.30, 56.72, -538.59, 44.6825788836, -55.6373574526],
      [-28.09, 46.20, 708.74, -18.5282204267, 50.7956016582],
      [41.85, 99.31, 329.24, 86.7538347651, 63.9348946472],
      [-5.30, 51.31, 344.10, 8.9596185705, 50.7989304520],
      [-74.13, 9.64, -65.94, -21.4198208183, 71.6196745044],
      [-36.07, -28.83, 440.57, 22.5306068008, -40.3061478833],
      [94.10, -2.71, 381.10, 88.7665189771, 31.3473955647],
      [51.02, -80.82, -311.22, 94.4113208338, -14.8800301888],
      [0.98, 59.59, -15.30, 16.6694613175, 57.2193809769],
      [27.89, 13.15, 69.82, -2.7215379558, 30.7142935969],
    ]

    tuples.each do |x, y, degrees, expected_x, expected_y|
      v = Vector2.new x, y
      actual = v.rotate degrees

      # Assert immutability
      assert_close(x, v.x, 1.0e-3, "Vector2.rotate seems to have altered the invoking vector, which was to be immutable. Method rotate is supposed to return a new value without changing the original vector")
      assert_close(y, v.y, 1.0e-3, "Vector2.rotate seems to have altered the invoking vector, which was to be immutable. Method rotate is supposed to return a new value without changing the original vector")

      # Assert rotation
      assert_close(expected_x, actual.x, 1.0e-3, "Vector2.rotate gave me an unexpected x-value for [#{v.x}, #{v.y}].rotate(#{degrees})")
      assert_close(expected_y, actual.y, 1.0e-3, "Vector2.rotate gave me an unexpected y-value for [#{v.x}, #{v.y}].rotate(#{degrees})")
    end
  end

  def test_vector2_length
    g = Random.new
    30.times do
      x = g.rand(-100.0..100.0)
      y = g.rand(-100.0..100.0)

      v = Vector2.new(x, y)
      result = v.length

      # Assert immutability
      assert_close(x, v.x, 1.0e-3, "Vector2.length seems to have altered the invoking vector, which was to be immutable. Method length is supposed to return a new value without changing the original vector")
      assert_close(y, v.y, 1.0e-3, "Vector2.length seems to have altered the invoking vector, which was to be immutable. Method length is supposed to return a new value without changing the original vector")

      # Assert quotient
      assert_close(Math.sqrt(x * x + y * y), result, 1.0e-4, "Vector2.length gave me an unexpected length for [#{v.x}, #{v.y}]")
    end
  end

  def test_vector2_plus
    g = Random.new
    30.times do
      x1 = g.rand(-100.0..100.0)
      y1 = g.rand(-100.0..100.0)
      x2 = g.rand(-100.0..100.0)
      y2 = g.rand(-100.0..100.0)

      v1 = Vector2.new(x1, y1)
      v2 = Vector2.new(x2, y2)
      result = v1 + v2

      # Assert immutability
      assert_close(x1, v1.x, 1.0e-3, "Vector2.+ seems to have altered the invoking vector, which was to be immutable. Method + is supposed to return a new vector without changing the original")
      assert_close(y1, v1.y, 1.0e-3, "Vector2.+ seems to have altered the invoking vector, which was to be immutable. Method + is supposed to return a new vector without changing the original")
      assert_close(x2, v2.x, 1.0e-3, "Vector2.+ seems to have altered the invoking vector, which was to be immutable. Method + is supposed to return a new vector without changing the original")
      assert_close(y2, v2.y, 1.0e-3, "Vector2.+ seems to have altered the invoking vector, which was to be immutable. Method + is supposed to return a new vector without changing the original")

      # Assert quotient
      assert_close(v1.x + v2.x, result.x, 1.0e-4, "Vector2.+ gave me an unexpected x-value for [#{v1.x}, #{v1.y}] + [#{v2.x}, #{v2.y}]")
      assert_close(v1.y + v2.y, result.y, 1.0e-4, "Vector2.+ gave me an unexpected y-value for [#{v1.x}, #{v1.y}] + [#{v2.x}, #{v2.y}]")
    end
  end

  def test_vector2_minus
    g = Random.new
    30.times do
      x1 = g.rand(-100.0..100.0)
      y1 = g.rand(-100.0..100.0)
      x2 = g.rand(-100.0..100.0)
      y2 = g.rand(-100.0..100.0)

      v1 = Vector2.new(x1, y1)
      v2 = Vector2.new(x2, y2)
      result = v1 - v2

      # Assert immutability
      assert_close(x1, v1.x, 1.0e-3, "Vector2.- seems to have altered the invoking vector, which was to be immutable. Method - is supposed to return a new vector without changing the original")
      assert_close(y1, v1.y, 1.0e-3, "Vector2.- seems to have altered the invoking vector, which was to be immutable. Method - is supposed to return a new vector without changing the original")
      assert_close(x2, v2.x, 1.0e-3, "Vector2.- seems to have altered the invoking vector, which was to be immutable. Method - is supposed to return a new vector without changing the original")
      assert_close(y2, v2.y, 1.0e-3, "Vector2.- seems to have altered the invoking vector, which was to be immutable. Method - is supposed to return a new vector without changing the original")

      # Assert quotient
      assert_close(v1.x - v2.x, result.x, 1.0e-4, "Vector2.- gave me an unexpected x-value for [#{v1.x}, #{v1.y}] - [#{v2.x}, #{v2.y}]")
      assert_close(v1.y - v2.y, result.y, 1.0e-4, "Vector2.- gave me an unexpected y-value for [#{v1.x}, #{v1.y}] - [#{v2.x}, #{v2.y}]")
    end
  end

  def test_vector2_times
    g = Random.new
    30.times do
      x = g.rand(-100.0..100.0)
      y = g.rand(-100.0..100.0)

      scalar = g.rand(-200.0..200.0)

      v = Vector2.new(x, y)
      product = v * scalar

      # Assert immutability
      assert_close(x, v.x, 1.0e-3, "Vector2.* seems to have altered the invoking vector, which was to be immutable. Method * is supposed to return a new vector without changing the original")
      assert_close(y, v.y, 1.0e-3, "Vector2.* seems to have altered the invoking vector, which was to be immutable. Method * is supposed to return a new vector without changing the original")

      # Assert quotient
      assert_close(x * scalar, product.x, 1.0e-4, "Vector2.* gave me an unexpected x-value for [#{x}, #{y}] * #{scalar}")
      assert_close(y * scalar, product.y, 1.0e-4, "Vector2.* gave me an unexpected y-value for [#{x}, #{y}] * #{scalar}")
    end
  end

  def test_vector2_divide
    g = Random.new
    30.times do
      x = g.rand(-100.0..100.0)
      y = g.rand(-100.0..100.0)

      # Get a non-zero denominator
      denominator = g.rand(-200.0..200.0)
      denominator += 2 if denominator.abs < 1.0e-3

      v = Vector2.new(x, y)
      quotient = v / denominator

      # Assert immutability
      assert_close(x, v.x, 1.0e-3, "Vector2./ seems to have altered the invoking vector, which was to be immutable. Method / is supposed to return a new vector without changing the original")
      assert_close(y, v.y, 1.0e-3, "Vector2./ seems to have altered the invoking vector, which was to be immutable. Method / is supposed to return a new vector without changing the original")

      # Assert quotient
      assert_close(x / denominator, quotient.x, 1.0e-4, "Vector2./ gave me an unexpected x-value for [#{x}, #{y}] / #{denominator}")
      assert_close(y / denominator, quotient.y, 1.0e-4, "Vector2./ gave me an unexpected y-value for [#{x}, #{y}] / #{denominator}")
    end
  end

  def test_vector2_normalize
    # g = Random.new
    # 30.times do
      # x = g.rand(-100.0..100.0)
      # y = g.rand(-100.0..100.0)

      # v = Vector2.new(x, y)
      # result = v.normalize

      # puts "[%.10f, %.10f, %.10f, %.10f]," % [x, y, result.x, result.y]
    # end

    tuples = [
      [41.2968342527, -34.0973507237, 0.7711214918, -0.6366880279],
      [-82.1789901203, -36.5566382369, -0.9136768045, -0.4064415048],
      [8.5204545632, -84.3787626958, 0.1004677420, -0.9949403162],
      [73.1103929806, 71.7615893020, 0.7136592375, 0.7004930355],
      [9.7274686218, -89.5908723461, 0.1079421650, -0.9941571752],
      [-54.9705663783, -58.5639425917, -0.6843840981, -0.7291216677],
      [46.7313898297, 53.8729963419, 0.6552624604, 0.7554012894],
      [-41.6766358830, -90.8722361230, -0.4168767358, -0.9089630285],
      [-10.7058666896, 10.3007158095, -0.7206106312, 0.6933399730],
      [19.6263734123, -82.2596104942, 0.2320765297, -0.9726975297],
      [59.5014430601, -47.2186431824, 0.7833191869, -0.6216197000],
      [5.7303743988, 74.7569117183, 0.0764292287, 0.9970750087],
      [12.3321634985, -54.6350997712, 0.2201794220, -0.9754593903],
      [42.0811636360, 74.1125282573, 0.4937590102, 0.8695987809],
      [-58.0661429860, 24.0334545969, -0.9239826189, 0.3824344649],
      [87.8695226265, -55.7455894193, 0.8444066309, -0.5357027550],
      [-84.9391871284, 14.0729080833, -0.9865509736, 0.1634538973],
      [-44.0187358011, 34.8612362849, -0.7839325305, 0.6208460257],
      [70.3650479041, -59.8113101612, 0.7619338704, -0.6476548287],
      [49.6319050791, -58.8340367404, 0.6448002573, -0.7643511158],
      [-25.2526095227, 5.1439456412, -0.9798773414, 0.1996005908],
      [-82.4946683206, 60.9385155270, -0.8043429793, 0.5941652731],
      [-81.9296658096, 71.1154046939, -0.7551884398, 0.6555077576],
      [65.4535143954, -49.0303733300, 0.8003506290, -0.5995322099],
      [95.4881518288, 48.9118812591, 0.8900308248, 0.4559003519],
      [-72.4859519656, -83.3527414942, -0.6562060729, -0.7545817317],
      [-33.9654087542, -56.8737924770, -0.5127312830, -0.8585491433],
      [62.8630121857, 25.5211716994, 0.9265535502, 0.3761628885],
      [99.8621132954, 77.8571120868, 0.7886376182, 0.6148582822],
      [-80.8574881859, 36.4654044897, -0.9115856467, 0.4111102148],
    ]

    tuples.each do |x, y, expected_x, expected_y|
      v = Vector2.new x, y
      actual = v.normalize

      # Assert immutability
      assert_close(x, v.x, 1.0e-3, "Vector2.normalize seems to have altered the invoking vector, which was to be immutable. Method normalize is supposed to return a new value without changing the original vector")
      assert_close(y, v.y, 1.0e-3, "Vector2.normalize seems to have altered the invoking vector, which was to be immutable. Method normalize is supposed to return a new value without changing the original vector")

      # Assert rotation
      assert_close(expected_x, actual.x, 1.0e-3, "Vector2.normalize gave me an unexpected x-value for [#{v.x}, #{v.y}].normalize")
      assert_close(expected_y, actual.y, 1.0e-3, "Vector2.normalize gave me an unexpected y-value for [#{v.x}, #{v.y}].normalize")

      assert_close(1.0, actual.length, 1.0e-3, "Vector2.normalize gave me an unexpected length for [#{v.x}, #{v.y}].normalize.length")
    end
  end

  def check_world path, expected, is_generating = false
    #, width, height, start, lookat, grid
    world = World.new path

    if is_generating
      obj = {}
      obj['dims'] = [world.width, world.height]
      obj['lookat'] = [world.lookat.x, world.lookat.y]
      obj['start'] = [world.start.x, world.start.y]
      obj['grid'] = []
      world.height.times do |r|
        world.width.times do |c|
          obj['grid'] << world[c, r]
        end
      end
      puts obj.inspect
    end

    assert_equal(expected['dims'][0], world.width, "I tried to load in a world from test #{path}, but I didn't get back the width I expected")
    assert_equal(expected['dims'][1], world.height, "I tried to load in a world from test #{path}, but I didn't get back the height I expected")
    assert_close(expected['start'][0], world.start.x, 1.0e-3, "I tried to load in a world from test #{path}, but I didn't get back the start x-value I expected")
    assert_close(expected['start'][1], world.start.y, 1.0e-3, "I tried to load in a world from test #{path}, but I didn't get back the start y-value I expected")
    assert_close(expected['lookat'][0], world.lookat.x, 1.0e-3, "I tried to load in a world from test #{path}, but I didn't get back the lookat x-value I expected")
    assert_close(expected['lookat'][1], world.lookat.y, 1.0e-3, "I tried to load in a world from test #{path}, but I didn't get back the lookat y-value I expected")

    expected_width = expected['dims'][0]
    expected_height = expected['dims'][1]

    expected_height.times do |r|
      expected_width.times do |c|
        assert_equal(expected['grid'][c + expected_width * r], world[c, r], "I tried to load in a world from test #{path}, but I didn't get back the cell number I expected for cell #{c}, #{r}")
        assert_equal(expected['grid'][c + expected_width * r] == 0, world.is_passage(Vector2.new(c, r)), "I tried to load in a world from test #{path}, but I didn't get back the value from is_passage that I expected for cell #{c}, #{r}")
        assert_equal(expected['grid'][c + expected_width * r] == 0, world.is_passage(Vector2.new(c + 0.9, r + 0.7)), "I tried to load in a world from test #{path}, but I didn't get back the value from is_passage that I expected for vector (#{c + 0.9}, #{r + 0.7})")
        assert_equal(expected['grid'][c + expected_width * r] == 0, world.is_passage(Vector2.new(c + 0.1, r + 0.8)), "I tried to load in a world from test #{path}, but I didn't get back the value from is_passage that I expected for vector (#{c + 0.1}, #{r + 0.8})")
      end
    end

    assert_equal(false, world.is_passage(Vector2.new(-1, -1)), "I tried to load in a world from test #{path}, but I didn't get back the value from is_passage that I expected for vector (-1, -1)")
    assert_equal(false, world.is_passage(Vector2.new(expected_width, expected_height)), "I tried to load in a world from test #{path}, but I didn't get back the value from is_passage that I expected for vector (#{expected_width}, #{expected_height})")

  end

  def test_worlds
    tests_dir = File.join(File.dirname(__FILE__), 'tests')
    tags = ['world1', 'world2', 'world3', 'world4', 'world5']
    expecteds = [
      {"dims"=>[3, 3], "lookat"=>[0.0, 1.0], "start"=>[1.5, 1.5], "grid"=>[1, 1, 1, 1, 0, 1, 1, 1, 1]},
      {"dims"=>[4, 3], "lookat"=>[-1.0, 0.0], "start"=>[2.5, 1.5], "grid"=>[1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1]},
      {"dims"=>[19, 24], "lookat"=>[0.0, 1.0], "start"=>[1.5, 1.5], "grid"=>[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]},
      {"dims"=>[39, 37], "lookat"=>[1.0, 0.0], "start"=>[1.5, 1.5], "grid"=>[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]},
      {"dims"=>[11, 11], "lookat"=>[0.0, 1.0], "start"=>[1.5, 1.5], "grid"=>[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]}
    ]

    tags.each_with_index do |tag, i|
      check_world "#{tests_dir}/#{tag}", expecteds[i], false
    end
  end

  def test_player_in_world1
    tests_dir = File.join(File.dirname(__FILE__), 'tests')
    path = File.join(tests_dir, 'world1')

    world = World.new path
    player = Player.new(world, 60)

    assert_close(1.5, player.position.x, 1.0e-3, "I tried to make a player in the world from test #{path}, but I didn't get back the start position x-value I expected")
    assert_close(1.5, player.position.y, 1.0e-3, "I tried to make a player in the world from test #{path}, but I didn't get back the start position y-value I expected")
    assert_close(0, player.lookat.x, 1.0e-3, "I tried to make a player in the world from test #{path}, but I didn't get back the lookat x-value I expected")
    assert_close(1, player.lookat.y, 1.0e-3, "I tried to make a player in the world from test #{path}, but I didn't get back the lookat y-value I expected")
    assert_close(60, player.fov, 1.0e-3, "I tried to make a player in the world from test #{path}, but I didn't get back the fov I expected")
    assert_close(1, player.right.x, 1.0e-3, "I tried to make a player in the world from test #{path}, but I didn't get back the right vector x-value I expected")
    assert_close(0, player.right.y, 1.0e-3, "I tried to make a player in the world from test #{path}, but I didn't get back the right vector y-value I expected")

    player.strafe 0.3
    assert_close(1.8, player.position.x, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, but I didn't get back the position x-value I expected")
    assert_close(1.5, player.position.y, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, but I didn't get back the position y-value I expected")

    player.advance 0.1
    assert_close(1.8, player.position.x, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, and then advance 0.1 units, but I didn't get back the position x-value I expected")
    assert_close(1.6, player.position.y, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, and then advance 0.1 units, but I didn't get back the position y-value I expected")

    player.advance -0.4
    assert_close(1.8, player.position.x, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, and then advance 0.1 units, and then advance -0.4 units, but I didn't get back the position x-value I expected")
    assert_close(1.2, player.position.y, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, and then advance 0.1 units, and then advance -0.4 units, but I didn't get back the position y-value I expected")

    player.strafe -0.7
    assert_close(1.1, player.position.x, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, and then advance 0.1 units, and then advance -0.4 units, and then strafe -0.7 units, but I didn't get back the position x-value I expected")
    assert_close(1.2, player.position.y, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, and then advance 0.1 units, and then advance -0.4 units, and then strafe -0.7 units, but I didn't get back the position y-value I expected")

    player.advance 1
    assert_close(1.1, player.position.x, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, and then advance 0.1 units, and then advance -0.4 units, and then strafe -0.7 units, and then over-advance 1 unit, but I didn't get back the position x-value I expected")
    assert_close(1.2, player.position.y, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, and then advance 0.1 units, and then advance -0.4 units, and then strafe -0.7 units, and then over-advance 1 unit, but I didn't get back the position y-value I expected")

    player.strafe 1
    assert_close(1.1, player.position.x, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, and then advance 0.1 units, and then advance -0.4 units, and then strafe -0.7 units, and then over-advance 1 unit, and then over-strafe 1 unit, but I didn't get back the position x-value I expected")
    assert_close(1.2, player.position.y, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, and then advance 0.1 units, and then advance -0.4 units, and then strafe -0.7 units, and then over-advance 1 unit, and then over-strafe 1 unit, but I didn't get back the position y-value I expected")

    player.rotate 90
    assert_close(-1, player.lookat.x, 1.0e-3, "I tried to make a player in the world from test #{path} rotate 90 degrees, but I didn't get back the lookat x-value I expected")
    assert_close(0, player.lookat.y, 1.0e-3, "I tried to make a player in the world from test #{path} rotate 90 degrees, but I didn't get back the lookat y-value I expected")

    player.rotate 90
    assert_close(0, player.lookat.x, 1.0e-3, "I tried to make a player in the world from test #{path} rotate 180 degrees, but I didn't get back the lookat x-value I expected")
    assert_close(-1, player.lookat.y, 1.0e-3, "I tried to make a player in the world from test #{path} rotate 180 degrees, but I didn't get back the lookat y-value I expected")

    player.rotate 90
    assert_close(1, player.lookat.x, 1.0e-3, "I tried to make a player in the world from test #{path} rotate 270 degrees, but I didn't get back the lookat x-value I expected")
    assert_close(0, player.lookat.y, 1.0e-3, "I tried to make a player in the world from test #{path} rotate 270 degrees, but I didn't get back the lookat y-value I expected")

    player.advance 0.8
    assert_close(1.9, player.position.x, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, and then advance 0.1 units, and then advance -0.4 units, and then strafe -0.7 units, and then rotate 270 degrees, and then advance 0.8 units, but I didn't get back the position x-value I expected")
    assert_close(1.2, player.position.y, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, and then advance 0.1 units, and then advance -0.4 units, and then strafe -0.7 units, and then rotate 270 degrees, and then advance 0.8 units, but I didn't get back the position y-value I expected")

    player.strafe -0.7
    assert_close(1.9, player.position.x, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, and then advance 0.1 units, and then advance -0.4 units, and then strafe -0.7 units, and then rotate 270 degrees, and then advance 0.7 units, and then strafe -0.7 units, but I didn't get back the position x-value I expected")
    assert_close(1.9, player.position.y, 1.0e-3, "I tried to make a player in the world from test #{path} strafe 0.3 units, and then advance 0.1 units, and then advance -0.4 units, and then strafe -0.7 units, and then rotate 270 degrees, and then advance 0.7 units, and then strafe -0.7 units, but I didn't get back the position y-value I expected")
  end
end
