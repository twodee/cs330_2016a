Do this every coding session:

  1. git pull twodee master
  2. git pull
  3. make your changes
  4. git add [changed files]
  5. git commit
  6. git push
  7. verify on Bitbucket
